# MLflow Project Template for Git Repo

This repository contains the template files to create an MLflow-Project compatible git repository as well some examples. The available templates are for conda environment and docker environment (for non-python training script). Both are the currently supported environment by MLflow in time of writing. 

## General Usage

1. Add the template files to your project's root or to an empty folder.
2. Modify the configurations. 

   If you put the template files into an empty folder, you can modify the configuration file to use codes from a remote git repository.

3. Add mlflow logging functions in your training/test scripts. The scripts in the remote git repository must also have these functions.

   Example:

```
import mlflow

mlflow.log_param("learning-rate", "0.5")
mlflow.log_param("optimizer", "Adam")

# Running the training scripts....
#
#

# Metric value must be numeric.
mlflow.log_metric("accuracy", 0.7)
mlflow.log_metric("precision", 0.65)

# You can log metric with the same key more than once.
mlflow.log_metric("accuracy", 0.9)
mlflow.log_metric("precision", 0.8)

# PYTHON-ONLY: mlflow.log_artifact will copy a file to the current run's artifact directory.
mlflow.log_artifact("output/model.hd5")
```

4. Execute ```mlflow run```  from CLI or use the start_mlflow.sh script from the project root

5. Access the MLflow UI to see the run log.


## Installation

Open the *project/[conda|docker]* folder for instructions on how to install each environments.

Optionally, add start_mlflow.sh to your project root to start the run.

## Examples

Examples can be found in *examples* folder.
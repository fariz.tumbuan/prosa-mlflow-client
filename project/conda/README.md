# Installation

This template file can be used in two ways:

1. Copy the the files in this folder to the root of your project and direct the MLflow to the current folder.
2. Copy the files into a folder and direct the MLflow to the git repository (requires ssh access).

## MLproject

```MLproject``` file specifies 3 things:

1. Project name :   The name of the project. It's advised to use the same name as the repository name.
2. conda_env    :   Path to the conda environment file. Default value refers to config/myenv.yaml.
3. entrypoints  :   The command to run the python script as well CLI argument.
    - Multiple entrypoints may be written. All entrypoints will be executed in order.
    - An entrypoint with the name **main** must exist. This entry point will be used as the default when no entry point was specified.
    - Parameters are optional. The example below shows the 4 available parameter types and how to set the default value.
    - The directory written in the default value of a path variable must exist.
    - An entrypoint must have exactly one command which runs a python or bash script.

Example:

```
name: mlflow-project-template

conda_env: config/myenv.yaml

entry_points:
  main:
    parameters:
      param1: {type: string, default: "String param"}
      param2: {type: float, default: 0.1}
      param3: {type: path, default: "config"}
      param4: {type: uri, default: "sftp:/0.0.0.0/}
    command: "python3 train.py {param1} {param2} {param3} {param4}"
```

## myenv.yaml

```myenv.yaml``` lists the dependencies required by the repository. When the script is launched, MLflow will create a virtual environment and install those dependencies.

Example:

```
name: environment_for_training

dependencies:
  # List of dependencies which are available in conda
  - python=3.6 # or 2.7
  - numpy
  - pandas
  - scikit-learn
  - tensorflow
  - gensim

  # Dependencies that are only available trough pip can be specified like below.
  # mlflow is required!
  - pip:
    - mlflow
```

    submitted_run = mlflow.projects.run(
            mlflow_conf["project_uri"],
            entry_point=mlflow_conf["entry_point"],
            experiment_name=mlflow_conf["experiment_name"],
            parameters=mlflow_conf["parameters"],
            synchronous=mlflow_conf["synchronous"],
    )
    mlflow.end_run()
```
#!/bin/bash

mlflow_server="http://127.0.0.1:5090"
run_api="api/2.0/mlflow/runs"
exp_api="api/2.0/mlflow/experiments"

ctype_json="Content-Type: application/json"

#exp_id=$MLFLOW_EXP_ID
#run_id=$MLFLOW_RUN_ID

exp_id=0
run_id="dummy_run"

# Arguments: url, json_data
function post_req() {
  if [ "$#" -eq 2 ]; then
    curl \
			--header "$ctype_json" \
	    --request POST \
  	  --data "$2" \
   		"$1"
  fi
}

# Arguments: key, value
function json_elm() {
	if [ "$#" -eq 2 ]; then
		echo '"'$1'":"'$2'"'
	fi
}

# Arguments: payload, key
# key example: "['key1']['key2']...['keyn']"
function py_json_code {
	if [ "$#" -ge 2 ]; then
		code="import json; print(json.loads('$(echo $1)')$2)"
		echo "$code"
	fi
}

# Arguments: exp_name
function create_exp() {
	if [ "$#" -eq 1 ]; then
    local url="$mlflow_server/$exp_api/create"
		local json_data="{$(json_elm "experiment_name" $1)}"
		echo "$(post_req $url $json_data)"
	fi
}

# Arguments: exp_id
function create_run() {
  if [ -n exp_id ]; then
    local url="$mlflow_server/$run_api/create"
		local json_data="{$(json_elm "experiment_id" $1)}"
    local req="$(post_req $url $json_data)"
		local payload_key="['run']['info']['run_id']"
		run_id=$(python3 -c "$(py_json_code "$req" "$payload_key")")
		echo "$run_id"
  fi
}

# Arguments: key, value
function log_param() {
  if [ "$#" -eq 2 ]; then
		local url="$mlflow_server/$run_api/log-parameter"
		local json_data="{$(json_elm "run_id" $run_id), $(json_elm "key" $1), $(json_elm "value" $2)}"
		echo $json_data
		local req="$(post_req $url $json_data)"
		echo "$req"
	else
		echo "Failed to log parameter"
	fi
}

echo $(log_param "before" "failed" "AS")
run_id=$(create_run 0)
echo $(log_param "after" "success")

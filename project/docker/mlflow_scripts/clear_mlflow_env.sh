#!/bin/bash

echo "Clearing all MLflow related environment variables..."
export MLFLOW_RUN_ID=""
export MLFLOW_TRACKING_API=""
export MLFLOW_EXPERIMENT_ID=""

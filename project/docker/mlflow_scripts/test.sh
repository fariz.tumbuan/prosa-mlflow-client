#!/bin/bash

. utils/post_req.sh
. utils/json_elm.sh

. clear_mlflow_env.sh
. create_run.sh
. log_param.sh "a param" "a value"
. log_metric.sh "a metric" 158
echo "run id: $MLFLOW_RUN_ID"
echo "tracking id: $MLFLOW_TRACKING_URI"
echo "exp id: $MLFLOW_EXPERIMENT_ID"

#!/bin/bash

mlflow_server=$MLFLOW_TRACKING_URI
if [ -z $mlflow_server ]; then
	echo "No MLflow tracking server config found. Setting to \"http://127.0.0.1:5000\"."
	mlflow_server="http://127.0.0.1:5000"
	export MLFLOW_TRACKING_URI="$mlflow_server"
fi

mlflow_run_api="api/2.0/mlflow/runs"
mlflow_exp_api="api/2.0/mlflow/experiments"

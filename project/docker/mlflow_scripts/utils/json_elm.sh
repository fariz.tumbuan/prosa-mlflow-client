#!/bin/bash

function json_elm() {
	if [ "$#" -eq 2 ]; then
		echo '"'$1'":"'$2'"'
	fi
}

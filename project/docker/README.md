# Installation

This template file can be used in two ways:

1. Copy the the files in this folder to the root of your project and direct the MLflow to the current folder.
2. Copy the files into a folder and direct the MLflow to the git repository (requires ssh access).

## MLproject

```MLproject``` file specifies 3 things:

1. Project name :   The name of the project. It's advised to use the same name as the repository name.
2. docker_env    :   

    - image: Image containing the training environment.
    - options: List of options to be used for running a container
        - flag: the option flag
        - value: value to be used
    
3. entrypoints  :   The command to run the python script as well CLI argument.
    - Multiple entrypoints may be written. All entrypoints will be executed in order.
    - An entrypoint with the name **main** must exist. This entry point will be used as the default when no entry point was specified.
    - Parameters are optional. The example below shows the 4 available parameter types and how to set the default value.
    - The directory written in the default value of a path variable must exist.
    - An entrypoint must have exactly one command which runs a python or bash script.

Example:

```
name: warungpintar

docker_env:
  image: "test-mlflow"
  options:
    - flag : "--mount"
      value : "source=warungpintar-data,target=/warungpintar/data"
    - flag : "--mount"
      value : "source=warungpintar-exp,target=/warungpintar/exp"
    - flag : "-w"
      value : "/mlflow/projects/code/warungpintar/src/"
    - flag : "--network"
      value : "host"

entry_points:
  main:
    command: "./run.sh"
```

## Dockerfile

The provided Dockerfile contains the minimal content for an Mlflow Project Image

Example:

```
FROM [IMAGE]

#This is fixed; MLproject will put the code in repo root here
ENV MLFLOW_PREFIX /mlflow/projects/code

#This path can be modified; Can be exported from the runner script
ENV MLFLOW_BASH_ROOT /mlflow/projects/code/mlflow_scripts

# These are the minimal depencies
RUN apt-get install python3-pip -y \
    && pip3 install mlflow \
    && apt-get install curl -y

```
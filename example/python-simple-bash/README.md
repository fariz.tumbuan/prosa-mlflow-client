# How to use:

1. Edit ```myenv.yaml``` file to configure the environment (mlflow must be included as dependencies).
2. Edit the ```MLproject file``` to configure the parameter and entry point.
3. Edit the ```mlflow_conf.yaml``` to configure the mlflow server uri and parameters.
4. insert the mlflow.log_param and mlflow.log_metric in the training script
5. Run mlflow_runner.py

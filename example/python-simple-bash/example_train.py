#import tensorflow as tf
#from keras import backend as K
#import sklearn as sk
#from gensim.models import Word2Vec
#import numpy
#import pandas
#import sklearn

import mlflow
import os
import random
import sys
import yaml

def main(argv):
#   param specified from mlfow run is automatically logged
#    for i in range(len(argv)):
#        mlflow.log_param("param{}".format(i), argv[i])
    mlflow.set_tag("mlflow.runName", "RUN NAME")
    metric1 = random.randrange(50, 80)
    metric2 = random.randrange(50, 80)
    
    mlflow.log_metric("metric1", metric1)
    mlflow.log_metric("metric2", metric2)


    artifact_dir = "artifacts/model/"
    os.makedirs(artifact_dir, exist_ok=True)
    with open(artifact_dir + "the_model", "w") as f:
        f.write("A model")
    mlflow.log_artifact(artifact_dir + "the_model")

if __name__ == "__main__":
    main(sys.argv)

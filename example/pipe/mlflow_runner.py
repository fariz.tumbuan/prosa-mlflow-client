#!/usr/bin/env python
import mlflow
import os
import sys
import yaml

if __name__ == "__main__":
    if not os.path.isfile("mlflow_conf.yaml"):
        print("Missing 'mlflow_conf.yaml")
        sys.exit()
    if not os.path.isfile("MLproject"):
        print("Missing 'MLproject file'")
        sys.exit()

    with open("mlflow_conf.yaml", "r") as f:
        mlflow_conf = yaml.safe_load(f)
    mlflow.tracking.set_tracking_uri(mlflow_conf["tracking_uri"])
    try:
        mlflow.create_experiment(mlflow_conf["experiment_name"])
    except mlflow.exceptions.RestException:
        pass
    
    submitted_run = mlflow.projects.run(
            mlflow_conf["project_uri"],
            entry_point=mlflow_conf["entry_point"],
            experiment_name=mlflow_conf["experiment_name"],
            parameters=mlflow_conf["parameters"],
            synchronous=mlflow_conf["synchronous"],
    )
    mlflow.end_run()

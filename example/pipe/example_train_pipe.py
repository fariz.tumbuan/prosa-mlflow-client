import mlflow
import os
import random
import sys

def main(argv):
    print("step0 argument:")
    print(argv)
    for i in range(len(argv)):
        mlflow.log_param("pipe_param{}".format(i), argv[i])
    metric1 = random.randrange(80, 100)
    metric2 = random.randrange(80, 100)
    
    mlflow.log_metric("metric1", metric1)
    mlflow.log_metric("metric2", metric2)


#    artifact_dir = "artifacts/model/"
#    os.makedirs(artifact_dir, exist_ok=True)
#    with open(artifact_dir + "the_model", "w") as f:
#        f.write("A model")
#    mlflow.log_artifact(artifact_dir + "the_model")

if __name__ == "__main__":
    main(sys.argv)

export MLFLOW_TRACKING_URI="http://127.0.0.1:9001"
export MLFLOW_S3_ENDPOINT_URL="http://127.0.0.1:9000"

exp_name="kaldi_test"
entry_point="main"

mlflow experiments create \
	--experiment-name $exp_name \
	|| echo "Experiment $exp_name already exists"


# mlflow run [options] uri
# example URI:
# 	"." : Current directory
#	"git@gitlab.com:fariz.tumbuan/mlflow-project-template.git" : git repository (with ssh access)

params=""
for p in $@; do
        params="$params $p "
done

mlflow run \
	--entry-point $entry_point \
	--experiment-name $exp_name \
	$params \
	.

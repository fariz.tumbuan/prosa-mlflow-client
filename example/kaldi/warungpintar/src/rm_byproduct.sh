exp_root="/warungpintar/exp/closed/"
exp_name=$1
exp_dir="$exp_root/$exp_name"
exp_temp="$exp_root/.temp_$exp_name"

# Save the important files
mkdir $exp_temp
cp $exp_dir/final.mdl $exp_temp/final.mdl
cp $exp_dir/final.occs $exp_temp/final.occs
cp $exp_dir/cmvn_opts $exp_temp/cmvn_opts
cp $exp_dir/tree $exp_temp/tree

# Delete the byproducts
rm -r $exp_dir

# Recreate experiment directory and move back the important files
mkdir $exp_dir
mv $exp_temp/final.mdl $exp_dir/final.mdl
mv $exp_temp/final.occs $exp_dir/final.occs
mv $exp_temp/cmvn_opts $exp_dir/cmvn_opts
mv $exp_temp/tree $exp_dir/tree

rmdir $exp_temp

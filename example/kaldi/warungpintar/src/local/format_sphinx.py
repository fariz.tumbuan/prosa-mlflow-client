#!/usr/bin/python3

import os
import re
import sys
import logging
from argparse import ArgumentParser
from ordered_set import OrderedSet
from collections import OrderedDict

log = logging.getLogger(__name__)
parser = ArgumentParser(
    description='Generate Kaldi training files from Sphinx.')
parser.add_argument('-i', '--input', help='Path to Sphinx project directory.')
parser.add_argument('-o', '--output', help='Path to Kaldi project directory.')
args = parser.parse_args()

reg = re.compile('^[A-Z]*')


def format_filename_wp(name):
    """
    Formats filenames to produce unique utterance indices. Provide one yourself
    if this one doesn't work for you.
    """
    n, spkr = name.split('_')
    idx = reg.match(n).span()[-1]
    setn = n[:idx]
    filen = n[idx:]
    return '_'.join([spkr.zfill(3), setn.zfill(3), filen.zfill(3)])


def format_sentence_wp(sent):
    """
    Formats sentences in transcription to remove leading <s> and </s>
    """
    return ' '.join([n for n in sent.split()][1:-1])


def main():
    try:
        assert os.path.exists(args.input)
        assert 'etc' in os.listdir(args.input)
        assert 'wav' in os.listdir(args.input)
        etc = os.path.join(args.input, 'etc')
        wav = os.path.join(args.input, 'wav')
    except Exception as e:
        log.error('Sphinx directories missing: %s' % e)
        sys.exit(1)

    # Get Sphinx project name
    sphx = open(os.path.join(etc, 'sphinx_train.cfg')).read().splitlines()[5]
    sphx = sphx.split()[-1][1:-2]

    # Get Sphinx files
    fileids = {}
    fileids['train'] = os.path.join(etc, sphx + '_train.fileids')
    fileids['test'] = os.path.join(etc, sphx + '_test.fileids_1')
    transcripts = {}
    transcripts['train'] = os.path.join(etc, sphx + '_train.transcription')
    transcripts['test'] = os.path.join(etc, sphx + '_test.transcription_1')

    # Make Kaldi dirs
    feat = os.path.join(args.output, 'feat')
    lex = os.path.join(args.output, 'dict')
    os.makedirs(feat, exist_ok=True)
    os.makedirs(lex, exist_ok=True)

    # Make wav.scp and spk2utt
    fmaps = {}
    for key in fileids.keys():
        os.makedirs(os.path.join(feat, key), exist_ok=True)
        lines = [line.rstrip() for line in open(fileids[key])]
        paths = [os.path.join(wav, line + '.wav') for line in lines]
        bases = [os.path.basename(line) for line in lines]
        names = [format_filename_wp(base) for base in bases]

        # wav.scp
        wavscp = [name + ' ' + path for name, path in zip(names, paths)]
        with open(os.path.join(feat, key + '/wav.scp'), 'w') as f:
            f.write('\n'.join(wavscp))
        for base, name in zip(bases, names):
            fmaps[base] = name

        # spk2utt
        spkrs = OrderedSet([name.split('_')[0] for name in names])
        spk2utt = OrderedDict((spkr, spkr) for spkr in spkrs)
        for name in names:
            spk2utt[name.split('_')[0]] += ' ' + name
        with open(os.path.join(feat, key + '/spk2utt'), 'w') as f:
            f.write('\n'.join([n for n in spk2utt.values()]))

    # Make text (transcription)
    for key in transcripts.keys():
        lines = [line.rstrip() for line in open(transcripts[key])]
        files = [fmaps[line.split()[-1][1:-1]] for line in lines]
        sents = [' '.join([w for w in line.split()[1:-2]]) for line in lines]
        text = [name + ' ' + sent for name, sent in zip(files, sents)]
        with open(os.path.join(feat, key + '/text'), 'w') as f:
            f.write('\n'.join(text))

    # Make phones
    phone = os.path.join(etc, sphx + '.phone')
    phones = [line.rstrip() for line in open(phone)]
    phones.extend(['<decak>', '<desis>', '<ketawa>', '<napas>', '<noise>'])
    with open(os.path.join(lex, 'nonsilence_phones.txt'), 'w') as f:
        f.write('\n'.join([p for p in phones if p != 'SIL']))
        f.write('\n')
    with open(os.path.join(lex, 'silence_phones.txt'), 'w') as f:
        f.write('sil')
        f.write('\n')
    with open(os.path.join(lex, 'optional_silence.txt'), 'w') as f:
        f.write('sil')
        f.write('\n')

    # Make lexicon
    dic = os.path.join(etc, sphx + '.dic')
    dics = [line.rstrip() for line in open(dic)]
    dics = ['<unk> <noise>'] + dics
    with open(os.path.join(lex, 'lexicon.txt'), 'w') as f:
        f.write('\n'.join([d for d in dics]))
        f.write('\n')


if __name__ == '__main__':
    main()

<unk> <noise>
abece a b e c e
abese a b e s e
abon a b o n
acua a c u a
adem a d e m
ades a d e s
aer a e r
air a i r
ais a i s
aktif a k t i f
aktip a k t i p
akua a k u a
akuarius a k u a r i u s
akwa a k w a
akwarius a k w a r i u s
ale a l e
alo a l o
aloe a l o e
american a m @ r i c a n
amerikan a m @ r i k a n
amplop a m p l o p
anget a ng e t
anggur a ng g u r
angin a ng i n
anti a n t i
apace a p a c e
apase a p a s e
apel a p @ l
api a p i
atu a t u
avolution a f o l u t i o n
ayam a y a m
badak b a d a k
bakar b a k a r
bakso b a k s o
balsam b a l s a m
balsem b a l s @ m
barlai b a r l ay
barlei b a r l e i
baso b a s o
batang b a t a ng
batangan b a t a ng a n
batuk b a t u k
bawang b a w a ng
belas b @ l a s
bendera b @ n d e r a
bendra b @ n d r a
beng b @ ng
beri b @ r i
berri b @ r r i
beruang b @ r u a ng
besar b @ s a r
better b @ t t @ r
bewri b e w r i
biasa b i a s a
biji b i j i
bilan b i l a n
bima b i m a
bintang b i n t a ng
bir b i r
biru b i r u
blak b l a k
blas b l a s
blek b l e k
blue b l u e
bode b o d e
bodre b o d r e
bodreks b o d r e k s
bol b o l
boled b o l e d
bolet b o l e t
bolpoin b o l p o i n
bos b o s
botol b o t o l
botolan b o t o l a n
box b o k s
bran b r a n
bren b r e n
bron b r o n
bronis b r o n i s
browen b r o w e n
buah b u a h
buaya b u a y a
bubuk b u b u k
bunga b u ng a
bungkus b u ng k u s
burs b u r s
burset b u r s @ t
bus b u s
butir b u t i r
cabe c a b @
caer c a e r
cair c a i r
cang c a ng
cange c a ng e
cap c a p
capucino c a p u c i n o
caramel c a r a m @ l
celup c e l u p
ceng c e ng
cenge c e ng e
cengku c e ng k u
ces c e s
cese c e s @
cingku c i ng k u
cis c i s
cise c i s @
citra c i t r a
clin c l i n
clup c l u p
cocacola c o c a c o l a
cococasew c o c o c a s e w
cococino c o c o c i n o
cocolatos c o c o l a t o s
cocomel c o c o m @ l
cocomelet c o c o m @ l e t
cocorang c o c o r a ng
cocoreng c o c o r e ng
cofesot c o f e s o t
cofesyot c o f e sy o t
cofi c o f i
cofie c o f i e
cokelat c o k e l a t
cokicoki c o k i c o k i
coklat c o k l a t
cokocasew c o k o c a s @ w
cokokasew c o k o k a s @ w
cokolatos c o k o l a t o s
colek c o l e k
cukur c u k u r
cul c u l
dai d ay
dan d a n
daring d a r i ng
dawni d a w n i
decolgen d e c o l g e n
dei d e i
deka d e k a
dekolgen d e k o l g e n
delapan d e l a p a n
deo d e o
dering d e r i ng
diapet d ' a p @ t
diare d ' a r e
dlapan d l a p a n
dobel d o b @ l
don d o n
donat d o n a t
doni d o n i
douni d o u n i
dring d r i ng
dua d u a
dun d u n
duo d u o
dus d u s
eipel e i p @ l
ekonomi e k o n o m i
ekstra e k s t r a
ektif e k t i f
ektra e k t r a
emas @ m a s
empat @ m p a t
emping @ m p i ng
enam @ n a m
enem @ n @ m
energen e n e r g e n
energi e n e r g i
enerjen e n e r j e n
enpio e n p i o
entrostop e n t r o s t o p
envio e n f i o
epel e p @ l
es e s
espe e s p e
esse e s s e
este e s t e
esti e s t i
fanta f a n t a
fantastik f a n t a s t i k
farfum f a r f u m
filter f i l t @ r
flag f l a g
fleg f l e g
florida f l o r i d a
floridina f l o r i d i n a
flu f l u
force f o r c @
fors f o r s
frappe f r a p p @
free f r i
fres f r e s
frestea f r e s t e a
fresti f r e s t i
frisian f r i s i a n
fruit f r u i t
frut f r u t
ful f u l
funtastik f e n t @ s t i k
fusien f u s i e n
fusion f u s i o n
ga g a
gaple g a p l e
garam g a r a m
garpit g a r p i t
garuda g a r u d a
gas g a s
gatsbi g a t s b i
gede g e d e
gege g e g e
gel g e l
gelas g e l a s
geliga g e l i g a
gemblong g e m b l o ng
gepe g e p @
geranium g e r a n i u m
getsbi g e t s b i
giletos g i l e t o s
ginger g i ng @ r
glu g l u
go g o
god g o d
goreng g o r e ng
gorengan g o r e ng a n
gpu g p u
granita g r a n i t a
grape g r a p @
gren g r e n
grep g r e p
grin g r i n
guapa g u a p a
guava g u a f a
gud g u d
gudang g u d a ng
gunung g u n u ng
hansaplas h a n s a p l a s
harum h a r u m
helai h e l ay
herbal h e r b a l
hijau h i j aw
hijo h i j o
hil h i l
hitam h i t a m
hom h o m
ice i c e
ijau i j aw
ijo i j o
in i n
indokafe i n d o k a f e
indokape i n d o k a p @
indomi i n d o m i
itam i t a m
item i t @ m
jagung j a g u ng
jahe j a h e
jambu j a m b u
jarcok j a r c o k
jarum j a r u m
jel j e l
jeli j e l i
jeruk j e r u k
ji j i
jinjer j i ny j @ r
jos j o s
juh j u h
kacang k a c a ng
kafe k a f e
kaki k a k i
kaleng k a l e ng
kalengan k a l e ng a n
kampak k a m p a k
kantong k a n t o ng
kap k a p
kapak k a p a k
kapal k a p a l
kape k a p e
kapucino k a p u c i n o
karamel k a r a m e l
karamelof k a r a m e l o f
karamelop k a r a m e l o p
karamelope k a r a m e l o p @
kardus k a r d u s
kare k a r e
kari k a r i
kartu k a r t u
kasih k a s i h
kayu k a y u
kecik k @ c i k
kecil k @ c i l
keju k e j u
kelasik k @ l a s i k
kelir k @ l i r
kencana k @ ny c a n a
kental k @ n t a l
ker k e r
keresek k @ r e s e k
kerim k @ r i m
kerimi k @ r i m i
keripik k @ r i p i k
kerupuk k @ r u p u k
keteng k @ t e ng
ketengan k @ t e ng a n
ketombe k e t o m b e
kis k i s
klasik k l a s i k
klean k l e a n
klin k l i n
klir k l i r
kofi k o f i
kofie k o f i e
kofimik k o f i m i k
kofimiks k o f i m i k s
kokakola k o k a k o l a
kokocino k o k o c i n o
kokokasew k o k o k a s @ w
kokolatos k o k o l a t o s
kokomel k o k o m e l
kokomelet k o k o m e l e t
kokorang k o k o r a ng
kokoreng k o k o r e ng
koleksion k o l e k s i o n
kolektion k o l e k t i o n
komik k o m i k
komiks k o m i k s
konimex k o n i m @ k s
kool k o o l
kopi k o p i
kopiko k o p i k o
kopimik k o p i m i k
kopimiks k o p i m i k s
kopisot k o p i s o t
kopisyot k o p i sy o t
korek k o r e k
kotak k o t a k
koyo k o y o
kraker k r a k @ r
krakers k r a k @ r s
kratindeng k r a t i n d e ng
kratingdaeng k r a t i ng d a e ng
kratingdeng k r a t i ng d e ng
kreker k r e k @ r
krekers k r e k @ r s
krem k r e m
kremi k r e m i
kretek k r e t e k
krim k r i m
krimi k r i m i
kuku k u k u
kulit k u l i t
kuning k u n i ng
kup k u p
kweni k w e n i
la l a
laig l a i g
laik l a i k
laits l a i t @ s
lan l a n
lang l a ng
lapa l a p a
lapan l a p a n
larutan l a r u t a n
late l a t e
laurier l aw r i e r
lava l a f a
lawrier l a w r i e r
leci l e c i
leici l e i c i
leicie l e i c i e
lembar l @ m b a r
lemen l e m @ n
lemon l e m o n
lex l e k s
leyci l e y c i
lidah l i d a h
lifeboy l i f @ b o y
lima l i m a
lime l i m @
lipeboy l i p @ b o y
lit l i t
lite l a i t
lits l i t s
long l o ng
lontong l o n t o ng
losion l o s i o n
lotion l o t i o n
luak l u a k
luh l u h
luki l u k i
luwak l u w a k
lux l u k s
ma m a
madu m a d u
madurasa m a d u r a s a
magnum m a g n u m
maid m a i d
mait m a i t
maizon m ay z o n
makasih m a k a s i h
maks m a k s
malboro m a l b o r o
malkis m a l k i s
mangga m a ng g a
manggo m a ng g o
mangkok m a ng k o k
manis m a n i s
marboro m a r b o r o
markisa m a r k i s a
marlboro m a r l b o r o
masak m a s a k
maxi m a k s i
meknum m @ k n u m
melati m @ l a t i
men m @ n
menggo m @ ng g o
menthol m @ n t h o l
mentol m @ n t o l
mentos m @ n t o s
merah m e r a h
mexi m e k s i
mi m i
migrain m i g r a i n
migran m i g r a n
migren m i g r e n
mijon m i j o n
mik m i k
miks m i k s
mil m i l
miled m i l e d
milek m i l e k
milo m i l o
milth m i l t e h a
mineral m i n @ r a l
miniyud m i n i y u d
miniyut m i n i y u t
minral m i n r a l
mint m i n t
minuwit m i n u w i t
minyak m i ny a k
mistik m i s t i k
mixagrip m i k s a g r i p
mizon m i z o n
mld e m e l d e
moca m o c a
mocacino m o c a c i n o
mocafrio m o c a f r i o
mocareta m o c a r e t a
moka m o k a
mokacino m o k a c i n o
mokafrio m o k a f r i o
mokareta m o k a r e t a
momogi m o m o g i
mpat m p a t
mx2000 e m e k s d u a n o l n o l n o l
nabati n a b a t i
nam n a m
napacin n a p a c i n
napasin n a p a s i n
nem n e m
neo n e o
neslait n e s l a i t
neslit n e s l i t
nipis n i p i s
niu n i u
non n o n
nu n u
nutri n u t r i
nutrigel n u t r i g e l
nutrijel n u t r i j e l
obeha o b e h a
oke o k e
oki o k i
orange o r a ng e
oren o r e n
oskadon o s k a d o n
paceo p a c e o
pack p a c k
pak p a k
palpi p a l p i
pan p a n
panadol p a n a d o l
panila p a n i l a
panjang p a ny j a ng
panta p a n t a
panter p a n t @ r
papermin p a p @ r m i n
paramex p a r a m @ k s
parfum p a r f u m
paseo p a s @ o
pasion p a s i o n
pat p a t
pek p @ k
pelastik p @ l a s t i k
pena p @ n a
pentin p @ n t i n
pentine p @ n t i n e
pepermen p @ p @ r m @ n
pepermin p @ p @ r m i n
peras p @ r a s
peres p @ r e s
permen p @ r m @ n
pilkita p i l k i t a
pilter p i l t @ r
ping p i ng
piring p i r i ng
pisau p i s aw
pit p i t
plastik p l a s t i k
plorida p l o r i d a
ploridina p l o r i d i n a
plu p l u
pokari p o k a r i
polkano p o l k a n o
ponstan p o n s t a n
pop p o p
popmi p o p m i
porce p o r c e
pors p o r s
power p o w e r
pree p r e e
pro p r o
profesional p r o f e s i o n a l
prokol p r o k o l
prokoled p r o k o l e d
promag p r o m a g
promah p r o m a h
prut p r u t
pucuk p u c u k
pulpen p u l p @ n
pulpi p u l p i
puluh p u l u h
puntastik p u n t a s t i k
putih p u t i h
puyer p u y e r
rambut r a m b u t
rasa r a s a
refil r e f i l
refres r e f r e s
rejois r e j o i s
relaxa r e l a k s a
remacil r e m a c i l
remasil r e m a s i l
remi r e m i
renceng r e ny c e ng
reno r e n o
renteng r e n t @ ng
repil r e p i l
repres r e p r e s
rexona r e k s o n a
rices r i c e s
ricis r i c i s
ricoco r i c o c o
ricoko r i c o k o
rijois r i j o i s
rikoko r i k o k o
rinso r i n s o
rol r o l
roma r o m a
sabun s a b u n
sacet s a c e t
sachet s a c e t
salonpas s a l o n p a s
sam s a m
sampoerna s a m p o e r n a
sampurna s a m p u r n a
san s a n
sanraise s a n r ay s @
sanrise s a n r i s @
sari s a r i
saset s a s @ t
satu s a t u
sebat s @ b a t
sebatang s @ b a t a ng
sebatangan s @ b a t a ng a n
sebelas s @ b @ l a s
sebiji s @ b i j i
seblas s @ b l a s
sebotol s @ b o t o l
sebotolan s @ b o t o l a n
sebox s @ b o k s
sebuah s @ b u a h
sebungkus s @ b u ng k u s
sebutir s @ b u t i r
secap s @ c a p
sedang s @ d a ng
sedeng s @ d e ng
seduh s @ d u h
sedus s @ d u s
segelas s @ g e l a s
sehelai s @ h e l ay
sekaleng s @ k a l e ng
sekalengan s @ k a l e ng a n
sekantong s @ k a n t o ng
sekap s @ k a p
sekardus s @ k a r d u s
sekeresek s @ k e r e s @ k
seketeng s @ k e t @ ng
seketengan s @ k e t e ng a n
sekotak s @ k o t a k
sekup s @ k u p
selembar s @ l e m b a r
selup s @ l u p
semangkok s @ m a ng k o k
sembilan s @ m b i l a n
sepack s @ p a c k
sepak s @ p a k
separoh s @ p a r o h
sepek s @ p @ k
sepelastik s @ p @ l a s t i k
sepiring s @ p i r i ng
sepirit s @ p i r i t
seplastik s @ p l a s t i k
sepuluh s @ p u l u h
serenceng s @ r e ny c e ng
serenteng s @ r e n t @ ng
sesacet s @ s a c e t
sesachet s @ s a c h e t
sesaset s @ s a s e t
sesetrip s @ s @ t r i p
sestrip s @ s t r i p
setablet s @ t a b l e t
setar s @ t a r
setengah s @ t @ ng a h
setile s @ t i l e
setrip s @ t r i p
sewit s @ w i t
signatur s i g n a t u r
sine s i n e
sinsui s i n s u i
sirsak s i r s a k
smoot s m u t
soda s o d a
soe s o e
sof s o f
sofel s o f e l
sopel s o p @ l
sorsop s a w @ r s o p
sosro s o s r o
soto s o t o
soursop s o u r s o p
special s p @ c i a l
spesial s p @ s i a l
spirit s p i r i t
standar s t a n d a r
star s t a r
stil s t i l
stle s t l e
straike s t r ay k e
strawberi s t r a w b @ r i
strewberi s t r e w b @ r i
strike s t r i k e
strip s t r i p
stroberi s t r o b @ r i
su s u
sufel s u f e l
sufle s u f l e
sunraise s u n r ay s @
sunrise s u n r i s @
sunsilek s u n s i l @ k
sunsilk s u n s i l k
super s u p @ r
superstar s u p @ r s t a r
surya s u r y a
susu s u s u
sweit s w e i t
swet s w e t
swit s w i t
syain sy a i n
tablet t a b l e t
tanggo t a ng g o
tanggung t a ng g u ng
tarek t a r e k
tarik t a r i k
tausen t aw s @ n
tauzen t aw z e n
te t @
tea t @ a
teh t @ h
telor t @ l o r
telur t @ l u r
ten t @ n
tenggo t @ ng g o
terima t @ r i m a
ti t i
tiga t i g a
tige t i g e
tokai t o k ay
tolak t o l a k
torabika t o r a b i k a
total t o t a l
trapel t r a p @ l
travel t r a f e l
tri t r i
tripel t r i p @ l
tropical t r o p i c a l
tropikal t r o p i k a l
tu t u
tujuh t u j u h
u u
ultra u l t r a
ultraflu u l t r a f l u
ungu u ng u
urut u r u t
vanila f a n i l a
vegeta f e g e t a
velpet f e l p e t
velvet f e l f e t
vera f e r a
vit f i t
volkano f o l k a n o
wa w a
wafer w a f e r
wan w a n
waper w a p @ r
wedang w e d a ng
whait w h a i t
wing w i ng
wit w i t
wite w i t @
yakul y a k u l
yu y u
yusi y u s i
yusu y u s u
yuzu y u z u
zing z i ng

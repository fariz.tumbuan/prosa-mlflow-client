# Kaldi reproduction of Warung Pintar Sphinx experiments

## Dependencies
- Kaldi [https://github.com/kaldi-asr/kaldi]
- Python >=3.5

## Usage
- Clone into `$KALDI_ROOT/egs/warungpintar/<repo>`
- Run `run.sh </path/to/sphinx/dir> <num-jobs> <stage>`
- Stages:
  + 1. Data preparation
  + 2. Monophone training
  + 3. Triphone training
- Experiment files are generated in `exp` and language models in `lang`

## Results
Setup 1: Use *only* Warung Pintar data for training
- Closed set (testing data -> training data)
  + Triphone: 29.90%
  + LDA + MLLT: 27.83%
  + SAT: 24.97%
- Validation set (testing data -> subset of training data not used in training)
  + Triphone: 35.5% (CMU Sphinx)
  + Triphone: 33.41%
  + LDA + MLLT: 29.95%
    SAT:  25.59%

Setup 2: Use Inti (spontan + read) data for training and WP data for testing
- "Open" set (testing data -> WP training data)

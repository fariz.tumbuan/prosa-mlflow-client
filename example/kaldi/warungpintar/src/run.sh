#!/bin/bash
# MLFLOW_PREFIX should be defined from the dockerfile
export MLFLOW_BASH_ROOT="/mlflow/projects/code/mlflow_scripts"
sphinx="/warungpintar/data/wp"
nj=10
nj_train=10
nj_test=6
stage=1 # Missing (/warungpintar/data/wp/etc) to execute stage 0
expr="/warungpintar/exp/closed"
train_dir="${MLFLOW_PREFIX}/warungpintar/src/feat/train"
test_dir="${MLFLOW_PREFIX}/warungpintar/src/feat/test"

. ./cmd.sh
. utils/parse_options.sh
. path.sh

if [ $stage -le 0 ]; then
  local/format_sphinx.py -i $sphinx -o .;
  utils/prepare_lang.sh dict '<unk>' lang lang;
  arpa2fst --disambig-symbol=#0 --read-symbol-table=lang/words.txt \
  $sphinx/etc/pos-word.lm lang/G.fst;
  for x in $train_dir $test_dir; do
    utils/spk2utt_to_utt2spk.pl $x/spk2utt > $x/utt2spk;
    utils/fix_data_dir.sh $x;
    steps/make_mfcc.sh --nj $nj $x || exit 1;
    steps/compute_cmvn_stats.sh $x || exit 1;
  done
fi

#####
# Experiment and run is created automatically by MLProject,
# so they should be created when testing locally.
#. $MLFLOW_BASH_ROOT/create_exp.sh "ASR CUY"
#. $MLFLOW_BASH_ROOT/create_run.sh
. $MLFLOW_BASH_ROOT/log_param.sh "boost" "123"
. $MLFLOW_BASH_ROOT/log_metric.sh "decode" 38.4
#####

## Monophone
#if [ $stage -le 1 ]; then 
#  mkdir -p $expr;
#  steps/train_mono.sh --boost-silence 1.25 --nj $nj $train_dir lang $expr/mono || exit 1;
#fi
#
## Skipping all decode process
#
## Triphone
#if [ $stage -le 2 ]; then
#  steps/align_si.sh --boost-silence 1.25 --nj $nj $train_dir lang $expr/mono \
#    $expr/mono_ali || exit 1;
#  steps/train_deltas.sh --boost-silence 1.25 200 1000 $train_dir lang \
#    $expr/mono_ali $expr/tri1 || exit 1;
#  utils/mkgraph.sh lang $expr/tri1 $expr/tri1/graph || exit 1;
# steps/decode.sh --nj $nj_test $expr/tri1/graph  $test_dir $expr/tri1/decode \
#   || exit 1;
#fi
#
## LDA + MLLT Triphone
#if [ $stage -le 3 ]; then
#  steps/align_si.sh --nj $nj $train_dir lang $expr/tri1 $expr/tri1_ali || exit 1;
#  steps/train_lda_mllt.sh --splice-opts "--left-context=3 --right-context=3" \
#    250 1500 $train_dir lang $expr/tri1_ali $expr/tri2b || exit 1;
#  utils/mkgraph.sh lang $expr/tri2b $expr/tri2b/graph || exit 1;
#  steps/decode.sh --nj $nj_test $expr/tri2b/graph $test_dir $expr/tri2b/decode \
#    || exit 1;
#fi
#
## LDA + MLLT + SAT
#if [ $stage -le 4 ]; then
#  steps/align_si.sh --nj $nj $train_dir lang $expr/tri2b $expr/tri2b_ali || \
#    exit 1;
#  steps/train_sat.sh 420 4000 $train_dir lang $expr/tri2b_ali $expr/tri3b || \
#    exit 1;
#  utils/mkgraph.sh lang $expr/tri3b $expr/tri3b/graph || exit 1;
#  steps/decode_fmllr.sh --nj $nj_test $expr/tri3b/graph $test_dir $expr/tri3b/decode \
#    || exit 1;
#fi
#
## Decode training data using most recent Inti spontan+read models
#
##if [ $stage -le 5 ]; then
## This part requires data in server which is too large
#  #utils/mkgraph.sh lang ../../asr/lstm_new/exp/tri_spontan_read \
#  #  ../../asr/lstm_new/exp/tri_spontan_read/graph_wp || exit 1
#  #steps/decode.sh --nj $nj --beam 20.0 --lattice-beam 10.0 --acwt 0.833 \
#  #  ../../asr/lstm_new/exp/tri_spontan_read/graph_wp \
#  #  $train_dir ../../asr/lstm_new/exp/tri_spontan_read/decode_wp
#
#  #utils/mkgraph.sh lang ../../inti/spontan/exp/tri \
#  #  ../../inti/spontan/exp/tri/graph_wp || exit 1;
#  #steps/decode.sh --nj $nj --beam 20.0 --lattice-beam 10.0 --acwt 0.8633 \
#  #  ../../inti/spontan/exp/tri/graph_wp $train_dir \
#  #  ../../inti/spontan/exp/tri/decode_wp
##fi
#
## Delete training byproducts
##rm_byproduct.sh mono;
##rm_byproduct.sh mono_ali;
##rm_byproduct.sh tri1;
##rm_byproduct.sh tri1_ali;
##rm_byproduct.sh tri2b;
##rm_byproduct.sh tri2b_ali;
##rm_byproduct.sh tri3b;

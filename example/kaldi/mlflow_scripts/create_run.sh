#!/bin/bash

. $MLFLOW_BASH_ROOT/api_uri.sh
. $MLFLOW_BASH_ROOT/utils/post_req.sh

mlflow_exp_id=$MLFLOW_EXPERIMENT_ID
if [ -z $mlflow_exp_id ]; then
	mlflow_exp_id=0
	export MLFLOW_EXPERIMENT_ID=$mlflow_exp_id
	echo "No experiment has been set! Setting experiment to Default."
fi

echo "Requesting a new run."
payload="{\"experiment_id\":\"$MLFLOW_EXPERIMENT_ID\"}"
req=$(post_req "${mlflow_server}/${mlflow_run_api}/create" "$payload")
echo "Run request response: $req"
payload_key="['run']['info']['run_id']"
code="import json; print(json.loads('$(echo $req)')$payload_key)"

export MLFLOW_RUN_ID=$(python3 -c "$code")

#!/bin/bash

. $MLFLOW_BASH_ROOT/api_uri.sh
. $MLFLOW_BASH_ROOT/utils/post_req.sh
. $MLFLOW_BASH_ROOT/utils/json_elm.sh

key=$1
value=$2

echo "Attempting to log parameter ($key : $value)..."
mlflow_run_id=$MLFLOW_RUN_ID
if [ -z $mlflow_run_id ]; then
	echo "No active run detected!"
	. create_run.sh
	mlflow_run_id=$MLFLOW_RUN_ID
fi

if [ -z key ] || [ -z value ]; then
	echo "Invalid argument!"
	exit -1
fi

payload="{$(json_elm "run_id" "$mlflow_run_id"), $(json_elm "key" "$key"), $(json_elm "value" "$value")}"
req=$(post_req "${mlflow_server}/${mlflow_run_api}/log-parameter" "$payload")
echo "response: $req"

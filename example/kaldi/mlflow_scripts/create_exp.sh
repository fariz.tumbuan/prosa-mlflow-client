#!/bin/bash

. $MLFLOW_BASH_ROOT/api_uri.sh
. $MLFLOW_BASH_ROOT/utils/post_req.sh

mlflow_exp_name=$1

echo "Creating new experiment..."
if [ -z $mlflow_exp_name ]; then
	echo "No experiment name provided!"
	exit -1
else
	payload="{\"name\":\"$mlflow_exp_name\"}"
	req=$(post_req "${mlflow_server}/${mlflow_exp_api}/create" "$payload")
	echo "Create experiment request response: $req"
fi

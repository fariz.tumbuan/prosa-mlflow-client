#!/bin/bash

header="Content-Type: application/json"

# Arguments: url, data
function post_req() {
	curl -s --header "$header" \
	--request POST \
	--data "$2" \
	"$1"
}

#!/usr/bin/env python
import mlflow
import os
import sys
import yaml

if __name__ == "__main__":
    config_file = "config/mlflow_config.yaml"
    mlproject_file = "MLproject"
    if not os.path.isfile(config_file):
        print("Missing 'mlflow_conf.yaml")
        sys.exit()
    if not os.path.isfile(mlproject_file):
        print("Missing 'MLproject file'")
        sys.exit()

    with open(config_file, "r") as f:
        mlflow_conf = yaml.safe_load(f)

    mlflow.tracking.set_tracking_uri(mlflow_conf["tracking_uri"])
    try:
        mlflow.create_experiment(mlflow_conf["experiment_name"])
    except mlflow.exceptions.RestException:
        pass
    
    submitted_run = mlflow.projects.run(
            mlflow_conf["project_uri"],
            entry_point=mlflow_conf["entry_point"],
            experiment_name=mlflow_conf["experiment_name"],
            parameters=mlflow_conf["parameters"],
            synchronous=mlflow_conf["synchronous"],
    )
    mlflow.end_run()
